<?php

namespace App\Controller\Admin;

use App\Entity\Student;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AvatarField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class StudentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Student::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('familyName'),
            TextField::new('lastName'),
            TextField::new('firstName'),
            TextField::new('register'),
            DateField::new('birthDate'),
            ChoiceField::new('gender')->setChoices([
                'Эр' => 'men',
                'Эм' => 'women',
                'Тодорхойгүй' => 'unknown',
            ]),
            ImageField::new('picture')
                ->setBasePath('uploads/')
                ->setUploadDir('public/uploads/')
                ->setUploadedFileNamePattern('[slug]-[contenthash].[extension]'),
            ChoiceField::new('currentLevel')->setChoices([
                '1-р зэрэг' => 'master_1',
                '2-р зэрэг' => 'master_2',
                '3-р зэрэг' => 'master_3',
            ]),
            TextField::new('experienceYear'),
            TextField::new('companyName'),
        ];
    }
}
