<?php

namespace App\Controller\Admin;

use App\Entity\Commission;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommissionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Commission::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('lastName'),
            TextField::new('firstName'),
            TextField::new('register'),
            ChoiceField::new('level')->setChoices([
                '1-р зэрэг' => 'master_1',
                '2-р зэрэг' => 'master_2',
                '3-р зэрэг' => 'master_3',
            ]),
        ];
    }
}
